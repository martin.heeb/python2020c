
from turtle import *

reset()

shape("turtle")

# Baumstamm

# Füllung vorbereiten
pencolor("brown")
fillcolor("brown")
begin_fill()
forward(20)
right(90)
forward(120)
right(90)
forward(20)
right(90)
forward(120)
end_fill()
# ende: Baumstamm

# Turtle richtig Drehen und in der Mitte positionieren
right(90)
forward(10)

pencolor("green")
fillcolor("green")
begin_fill()
circle(80)
end_fill()