#import des Moduls
from turtle import*
#Erstellen der Funktion für das Kreuz
def kreuz():
    penup()
    goto(-200,-200)
    pendown()
    #Konfiguration der Turtle
    fillcolor("red")
    pencolor("red")
    pensize(1)
    
    #Berechnung der Dimensionen
    balkenlaenge = 400/32*7
    balkenbreite = 400/32*6
    #Zeichnen der Grundfläche
    begin_fill()
    forward(400)
    left(90)
    forward(400)
    left(90)
    forward(400)
    left(90)
    forward(400)
    end_fill()
    
    #Positionierung für Kreuz
    left(90)
    forward(balkenbreite+balkenlaenge)
    left(90)
    forward(balkenbreite+balkenlaenge)
    left(180)
    
    #Zeichnen des Kreuzes
    begin_fill()
    fillcolor("white")
    pencolor("white")
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    end_fill()
    
    
    
    #Turtle ausblenden
    hideturtle()
    
#Aufrufen der Funktion    
kreuz()
