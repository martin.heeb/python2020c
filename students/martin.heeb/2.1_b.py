from turtle import*

def quadrat(seitenlaenge, fuellfarbe, stiftfarbe, stiftdicke):
    fillcolor(fuellfarbe)
    pencolor(stiftfarbe)
    pensize(stiftdicke)
    begin_fill()
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    end_fill()
    left(90)
    forward(seitenlaenge - seitenlaenge/5)
    right(15)
    
    
right(15)
quadrat(100, "pink", "red", 5)
quadrat(100, "yellow", "red", 5)
quadrat(100, "cyan", "red", 5)
quadrat(100, "lime", "red", 5)
quadrat(100, "blue", "red", 5)
