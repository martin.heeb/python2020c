#import des Moduls
from turtle import*
#Erstellen der Funktion für das Kreuz
def kreuz():
    
    #input der seitengrösse
    seitenlaenge = numinput("Eingabefenster", "Wie gross soll eine Seite in Anzahl Pixel sein?")
                         
    penup()
    goto(-200,-200)
    pendown()
    #Konfiguration der Turtle
    fillcolor("red")
    pencolor("red")
    pensize(1)
    
    #Berechnung der Dimensionen
    balkenlaenge = seitenlaenge/32*7
    balkenbreite = seitenlaenge/32*6
    #Zeichnen der Grundfläche
    begin_fill()
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    left(90)
    forward(seitenlaenge)
    end_fill()
    
    #Positionierung für Kreuz
    left(90)
    forward(balkenbreite+balkenlaenge)
    left(90)
    forward(balkenbreite+balkenlaenge)
    left(180)
    
    #Zeichnen des Kreuzes
    begin_fill()
    fillcolor("white")
    pencolor("white")
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    
    right(90)
    forward(balkenlaenge)
    left(90)
    forward(balkenbreite)
    left(90)
    forward(balkenlaenge)
    end_fill()
    
    
    
    #Turtle ausblenden
    hideturtle()
    
#Aufrufen der Funktion    
kreuz()

