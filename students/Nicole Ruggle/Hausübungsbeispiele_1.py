#Nicole Ruggle, Hausübungsbeispiele auf den 16.09.2020, adhoc-Übungen.
#1.1 a und b

print("Hans", "Muster")
print("Blablastrasse", "17")
print("8047", "Zürich")
print([1+2+3+4+5], [1-2-3-4-5], [1/2/3/4/5], [1*2*3*4*5])

# 1.2 a

from turtle import*
reset()

pensize(3)

forward(100)
right(90)
forward(100)
right(90)
forward(100)
right(90)
forward(100)

#1.2 e

from turtle import*

reset()
pensize (5)
pencolor("blue")

left (180)
forward(100)
right(120)
forward(100)
right(120)
forward(100)

pencolor("red")
left(120)
forward(100)
right(120)
forward(100)
right(120)
forward(100)

pencolor("lightgreen")
left(120)
forward(100)
right(120)
forward(100)
right(120)
forward(100)

#1.3 b - sorry für die Winkel :/

from turtle import*

reset()
pensize (5)
pencolor("red")

fillcolor("blue")
begin_fill()
right(70)
forward(100)
right(110)
forward(100)
right(90)
forward(100)
right(90)
forward(65)
end_fill()

fillcolor("lime")
begin_fill()
left(15)
forward(85)
right(80)
forward(100)
right(95)
forward(100)
end_fill()

fillcolor("cyan")
begin_fill()
right(180)
forward(100)
left(90)
forward(20)
left(-140)
forward(70)
right(95)
forward(80)
right(80)
forward(110)
end_fill()

fillcolor("yellow")
begin_fill()
right(180)
forward(105)
right(90)
forward(90)
right(100)
forward(100)
right(75)
forward(80)
end_fill()

fillcolor("magenta")
begin_fill()
right(180)
forward(80)
right(75)
forward(80)
right(90)
forward(65)
end_fill()

