# Simon Mettler

import turtle as t

t.reset()

t.pensize(5)
t.pencolor('blue')
t.lt(45)
t.forward(100)
t.pencolor('red')
t.rt(90)
t.forward(100)
t.pencolor('cyan')
t.lt(90)
t.forward(100)
t.pencolor('black')
t.rt(90)
t.forward(100)